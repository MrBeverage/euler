package org.beverage.euler.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.beverage.euler.helpers.MathHelper;
import org.beverage.euler.iterators.PrimeIterator;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;

import lombok.Data;
import lombok.Getter;

@Data
public class PrimeVector implements Iterable<Long> {

    @Data
    public static class PrimeFactor {
        long prime;
        long count;
        
        public void incrementCount() {
            count++;
        }

        public PrimeFactor(long prime, long count) {
            this.prime = prime;
            this.count = count;
        }
    }

    @Getter Long value = 1l;
    @Getter Long factorCount = 1l;
    @Getter TreeMap<Long, PrimeFactor> factors = new TreeMap<>();
    
    public PrimeVector() {
        factors.put(1l, new PrimeFactor(1l, 1l));
    }
    
    public PrimeVector(Collection<PrimeVector> vectors) {
        this();
        
        for (PrimeVector vector : vectors) {
            addVector(vector);
        }
    }
    
    public static PrimeVector fromFactors(Long[] factors) {
        return fromFactors(Arrays.asList(factors));
    }
    
    public static PrimeVector fromFactors(Collection<Long> factors) {
        PrimeVector vector = new PrimeVector();
        
        for (long factor : factors) {
            vector.addVector(MathHelper.getPrimeFactors(factor));
        }
        
        return vector;
    }
    
    public static PrimeVector fromPowerVector(Long[] powers) {
        return fromPowerVector(powers);
    }
    
    public static PrimeVector fromPowerVector(List<Long> powers) {
        PrimeVector vector = new PrimeVector();
        
        PrimeIterator iterator = new PrimeIterator();
        long prime = 1l;
        
        for (int i=0; i<powers.size(); i++) {
            prime = iterator.next();
            if (powers.get(i) < 0) {
                throw new IllegalArgumentException("Exponent must be greater than zero.");
            }
            
            if (powers.get(i) != 0) {
                vector.addPrime(new PrimeFactor(prime, powers.get(i)));
            }
        }
        
        return vector;
    }
    
    public void addPrime(long prime) {
        addPrime(new PrimeFactor(prime, 1l));
    }
    
    public void addPrime(PrimeFactor primeFactors) {
        PrimeFactor existingPrimeData = factors.get(primeFactors.getPrime());
        
        if (existingPrimeData == null) {
            factors.put(primeFactors.getPrime(), primeFactors);
        } else {
            existingPrimeData.setCount(existingPrimeData.getCount() + primeFactors.getCount());
        }

        factorCount += primeFactors.getCount();
        this.value *= (long) Math.pow(primeFactors.getPrime(), primeFactors.getCount());
    }
    
    public void subtractPrime(long prime) {
        subtractPrime(new PrimeFactor(prime, 1l));
    }
    
    public void subtractPrime(PrimeFactor primeFactors) {
        PrimeFactor existingPrimeData = factors.get(primeFactors.getPrime());
        
        if (existingPrimeData == null || existingPrimeData.getCount() < primeFactors.getCount()) {
            throw new IllegalArgumentException();
        } else {
            existingPrimeData.setCount(existingPrimeData.getCount() - primeFactors.getCount());
        }
        
        factorCount -= primeFactors.getCount();
        this.value /= (long) Math.pow(primeFactors.getPrime(), primeFactors.getCount());
    }
    
    public PrimeFactor getFactor(long prime) {
        return factors.get(prime);
    }
    
    public void setFactor(long prime, long multiple) {
        setFactor(new PrimeFactor(prime, multiple));
    }
    
    public void setFactor(PrimeFactor primeData) {
        factors.put(primeData.getPrime(), primeData);
        rebuildValue();
    }
    
    private void rebuildValue() {
        value = 1l;
        for (PrimeFactor factor : factors.values()) {
            value *= (long) Math.pow(factor.getPrime(), factor.getCount());
        }
    }
    
    public Multiset<Long> getFactorSet() {
        Multiset<Long> factorSet = HashMultiset.create();
        for (PrimeFactor data : factors.values()) {
            for (int i=0; i<data.getCount(); i++) {
                factorSet.add(data.getPrime());
            }
        }
        return factorSet;
    }
    
    public void addVector(PrimeVector vector) {
        for (PrimeFactor data : vector.getFactors().values()) {
            addPrime(data);
        }
    }
    
    public void subtractVector(PrimeVector vector) {
        for (PrimeFactor data : vector.getFactors().values()) {
            subtractPrime(data);
        }
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        
        builder.append(getValue() + " = ");
        for (Entry<Long, PrimeFactor> data : getFactors().entrySet()) {
            PrimeFactor prime = data.getValue();
            if (prime.getPrime() != 1l) {
                builder.append(prime.getPrime());
                if (prime.getCount() > 1) {
                    builder.append("^");
                    builder.append(prime.getCount());
                }
                builder.append(" * ");
            }
        }
        
        String result = builder.toString();
        result = result.substring(0, result.length() - 3);
        return result;
    }

    @Override
    public Iterator<Long> iterator() {
        List<Long> factors = new ArrayList<>();
        
        for (PrimeFactor data : getFactors().values()) {
//            if (data.getPrime() == 1l) {
//                continue;
//            }
//            
            for (int i=0; i<data.getCount(); i++) {
                factors.add(data.getPrime());
            }
        }
        
        return factors.iterator();
    }
}
