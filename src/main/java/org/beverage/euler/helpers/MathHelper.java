package org.beverage.euler.helpers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.beverage.euler.iterators.PrimeIterator2;
import org.beverage.euler.model.PrimeVector;

public class MathHelper {

    public static long reverseBase10Number(long number) {
        long sign = (long) Math.signum(number);
        if (sign == -1) {
            number = (long) Math.abs(number);
        }

        long order = (long) Math.log10((double) number) + 1;
        long result = 0l;

        for (long i = 0; i < order; i++) {
            long temp = number % (long) Math.pow(10, i + 1);
            temp /= (long) Math.pow(10, i);
            temp *= (long) Math.pow(10, order - i - 1);
            result += temp;
        }

        return sign * result;
    }
    
    public static boolean isPrime(long number) {
        PrimeVector vector = getPrimeFactors(number);
        return vector.getFactorSet().size() == 2;
    }
    
    public static PrimeVector getPrimeFactors(long number) {
        PrimeVector vector = new PrimeVector();
        
        while (number > 1) {
            //PrimeIterator2 iter = new PrimeIterator2((int) Math.ceil(Math.sqrt((double) number))); 
            PrimeIterator2 iter = new PrimeIterator2((int)number);
            
            do {
                long prime = iter.next();
                if (number % prime == 0) {
                    number /= prime;
                    vector.addPrime(prime);
                    break;
                }
            } while (iter.hasNext());
        }
        
        return vector;
    }
    
    public static List<List<Long>> combinations(int n, int k) {
        List<Long> indicies = new LinkedList<>();
        for (long i=0; i<n; i++) {
            indicies.add(i);
        }
        
        List<List<Long>> combinations = new ArrayList<>();
        List<Long> lastCombination = indicies.subList(0, k);
        
        combinations.add(lastCombination);
        
        for (int i=0; i < nChooseK(n, k); i++) {
            //  Increment from last index out inwards.
            for (int j = k - 1; j != 0; j--) {
                long value = lastCombination.get(j);
                
                if (value + 1 == n - j) {
                    
                }
            }
        }
        
        return combinations;
    }
    
    public static long nChooseK(long n, long k) {
        double result = 1.0;
        for (long i=1; i<=k; i++) {
            result *= (double) (n + 1 - i) / (double) i;
        }
        return (long) result;
    }
    
    public static long factorial(long n) {
        long result = 1l;
        for (long i=1; i<=n; i++) {
            result *= i;
        }
        return result;
    }
    
    public static long primesUnder(long number) {
        //  Only holds for x <= 55!
        double dNumber = number;
        return (long) Math.ceil(dNumber / (Math.log(dNumber) - 4));
    }
    
    public static long simpleSummation(long n) {
        return n * (n + 1) / 2;
    }
    
    public static int sumIntCollection(Collection<Integer> collection) {
        int sum = 0;
        for (Integer i : collection) {
            sum += i;
        }
        return sum;
    }
    
    public static long sumLongCollection(Collection<Long> collection) {
        long sum = 0l;
        for (Long l : collection) {
            sum += l;
        }
        return sum;
    }
    
    public static long collectionProduct(Collection<Long> collection) {
        long product = 1l;
        for (long l : collection) {
            product *= l;
        }
        return product;
    }
}
