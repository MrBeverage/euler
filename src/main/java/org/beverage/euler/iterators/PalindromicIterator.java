package org.beverage.euler.iterators;

import java.util.Iterator;

import org.beverage.euler.helpers.MathHelper;

public class PalindromicIterator implements Iterator<Long> {

    private long value;
    private final long direction;
    
    public PalindromicIterator(long startValue) {
        this.value     = startValue;
        this.direction = -1;
    }
    
    public PalindromicIterator(long startValue, long direction) {
        this.value     = startValue;
        this.direction = direction;
    }

    @Override
    public boolean hasNext() {
        return direction == 1 ? value < Long.MAX_VALUE : value > Long.MIN_VALUE;
    }

    @Override
    public Long next() {
        if (value == 0) {
            //  Special case: value == 0.
            value += direction;
            return value;
        }
        
        long sign  = (long) Math.signum(value);
        long order = (long) Math.log10(sign * value);
        
        if (order == 0) {
            // Special case: order zero. (-9 <-> 9)
            value += direction;
            return value;
        }
        
        long left  = sign * value / (long) Math.pow(10, (order - 1) / 2 + 1);
        long leftOrder = (long) Math.log10(left);
        long reverseLeft = MathHelper.reverseBase10Number(left);
        long right = sign * value % (long) Math.pow(10, order / 2 + 1);
        
        if (direction * sign == 1) {
            //  Magnitude is increasing - increment left if left mirror <= right, else mirror as-is.
            if (reverseLeft <= right) {
                left += sign * direction;
            }
        } else {
            //  Magnitude is decreasing - increment left if left mirror >= right, else mirror as-is.
            if (reverseLeft >= right) {
                left += sign * direction;
            }
        }

        long newLeftOrder = (long) Math.log10(left);
        
        //  Check for order of magnitude changes.
        if (direction * sign == -1 && (newLeftOrder < leftOrder || newLeftOrder == Long.MIN_VALUE)) {
            value = sign * getMaxPalindromeForOrder(order - 1);
        } else if (direction * sign == 1 && newLeftOrder > leftOrder) {
            value = sign * getMinPalindromeForOrder(order + 1);
        } else {
            value = sign * mergeLeftRight(left, MathHelper.reverseBase10Number(left), order);
        }
        
        return value;
    }

    public long mergeLeftRight(long left, long right, long order) {
        long result = 0l;
        long halfOrder = (order + 1) / 2 + 1;

        if (order % 2 == 0) {
            right %= (long) Math.pow(10, halfOrder - 1);
        }
        
        left *= (long) Math.pow(10, halfOrder - 1);
        result = left + right;
        
        return result;
    }
    
    public long getMaxPalindromeForOrder(long order) {
        long result = 0l;

        for (int i = 0; i < order + 1; i++) {
            result += 9 * (long) Math.pow(10, i);
        }

        return result;
    }

    public long getMinPalindromeForOrder(long order) {
        return (long) Math.pow(10, order) + (order > 0 ? 1 : 0);
    }
}
