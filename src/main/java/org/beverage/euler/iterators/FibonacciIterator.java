package org.beverage.euler.iterators;

import java.util.Iterator;

public class FibonacciIterator implements Iterator<Long> {

    private long current = 0;
    private long previous = 0;

    public boolean hasNext() {
        return current < Long.MAX_VALUE;
    }

    public Long next() {
        if (current == 0) {
            current = 1;
        } else {
            long next = previous + current;
            previous = current;
            current = next;
        }

        return current;
    }
}
