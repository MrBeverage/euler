package org.beverage.euler.iterators;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.beverage.euler.helpers.MathHelper;

public class CombinatoricIterator<T extends Comparable<T>> implements Iterator<List<T>> {

    private final LinkedList<T> elements;
    private final int k;
    private final long maxCombinations;
    
    private boolean first = false;
    private long combinations;
    
    public CombinatoricIterator(Collection<T> elements, int k) {
        this.elements = new LinkedList<T>(elements);
        this.k = k;
        this.maxCombinations = MathHelper.nChooseK(elements.size(), k);
    }
    
    @Override
    public boolean hasNext() {
        return combinations < maxCombinations;
    }

    @Override
    public List<T> next() {
        combinations++;
        
        if (elements.size() == 0 || elements.size() == 1 || elements.size() == k) {
            first = true;
            return elements;
        }
        
        if (first == false) {
            first = true;
            return elements.subList(0, k);
        }
        
        int i1 = k;
        int i2 = elements.size() - 1;
        
        while (i1 != 0) {
            if (elements.get(--i1).compareTo(elements.get(i2)) == -1) {
                int j = k;
                while (elements.get(i1).compareTo(elements.get(j)) != -1) {
                    j++;
                }
                swapValues(elements, i1, j);
                i1++;
                j++;
                i2 = k;
                rotateSubArray(elements, i1, j, elements.size());
                while (j < elements.size()) {
                    j++;
                    i2++;
                }
                rotateSubArray(elements, k, i2, elements.size());
                return elements.subList(0, k);
            }
        }
        
        rotateSubArray(elements, 0, k, elements.size());
        return elements.subList(0, k);
    }
    
    private void swapValues(LinkedList<T> list, int i, int j) {
        T temp = list.get(i);
        list.set(i, list.get(j));
        list.set(j, temp);
    }
    
    public LinkedList<T> rotateSubArray(LinkedList<T> list, int startPos, int pivot, int endPos) {
        if (pivot == endPos) return list;
        
        T pivotObject = list.get(pivot);
        T startObject = list.get(startPos);
        
        while (startObject.equals(pivotObject) == false) {
            list.offerLast(startObject);
            list.remove(startPos);
            startObject = list.get(startPos);
        }
        
        return list;
    }
}
