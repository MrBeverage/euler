package org.beverage.euler.iterators;

import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

import lombok.Data;

public class PrimeIterator implements Iterator<Long> {

    @SuppressWarnings("serial")
    private static final ConcurrentHashMap<Long, PrimeData> primes = new ConcurrentHashMap<Long, PrimeData>() {
        {
            put(0l, new PrimeData(2l, 2l));
        }
    };
    private static Long lastChecked = 2l;
    
    @Data
    private static class PrimeData {
        long value;
        long multiple;

        public PrimeData(long value, long multiple) {
            this.value = value;
            this.multiple = multiple;
        }
    }

    private long lastIndex = 0l;

    public boolean hasNext() {
        return true;
    }

    public Long next() {
        if (primes.get(lastIndex + 1) == null) {
            primes.computeIfAbsent(lastIndex + 1,
                    (index) -> computeNextPrime(index));
        }

        return primes.get(lastIndex++).getValue();
    }

    public String dumpPrimes() {
        return Arrays.toString(primes.values().toArray());
    }

    private static PrimeData computeNextPrime(long index) {

        boolean foundNewPrime = false;
        
        while (foundNewPrime == false) {
            lastChecked++;

            for (long primeValue : primes.keySet()) {
                PrimeData prime = primes.get(primeValue);
                long multiple = prime.getMultiple();
                long value = prime.getValue();
                
                if (lastChecked == multiple) {
                    prime.setMultiple(multiple + value);
                    foundNewPrime = false;
                    break;
                }

                if (lastChecked > multiple) {
                    prime.setMultiple(multiple + value);
                    foundNewPrime = false;
                }

                foundNewPrime = true;
            }
        }

        PrimeData newPrime = new PrimeData(lastChecked, lastChecked);
        return newPrime;
    }
}
