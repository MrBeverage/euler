package org.beverage.euler.iterators;

import java.util.Iterator;

public class MultipleIterator implements Iterator<Long> {

    private long base;
    private long current = 0;
    private long max;

    public MultipleIterator(long base, long max) {
        this.base = base;
        this.max = max;
    }

    public boolean hasNext() {
        return (current + 1) * base < max;
    }

    public Long next() {
        return (++current * base);
    }

    public void remove() {
        --current;
    }
}
