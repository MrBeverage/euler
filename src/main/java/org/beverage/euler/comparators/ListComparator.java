package org.beverage.euler.comparators;

import java.util.Comparator;
import java.util.List;

public class ListComparator<T extends Comparable<T>> implements Comparator<List<T>> {

    @Override
    public int compare(List<T> o1, List<T> o2) {
        if (o1.size() < o2.size()) {
          return -1;
      } else if (o2.size() > o1.size()) {
          return 1;
      } else {
          for (int i=0; i<o1.size(); i++) {
              if (o1.get(i).compareTo(o2.get(i)) < 0) {
                  return -1;
              } else if (o1.get(i).compareTo(o2.get(i)) > 0) {
                  return 1;
              }
          }
      }
      
      return 0;
    }
}
