package org.beverage.euler.problems;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
//import java.util.stream.Collectors;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import org.beverage.euler.comparators.ListComparator;
import org.beverage.euler.helpers.MathHelper;
import org.beverage.euler.iterators.CombinatoricIterator;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class P201UniqueSumSubset {

    private static int MIN_PROBLEM_SIZE = 5;
    //private static int PROBLEM_RANGE = 12;
    
    @ToString
    @AllArgsConstructor
    public class Composition implements Comparable<Composition> {
        @Getter Integer squaresUsed;
        @Getter Integer sum;
        @Getter List<Composition> baseCompositions;
        
        public Composition() {
            this.baseCompositions = new ArrayList<>();
            this.squaresUsed = 0;
            this.sum = 0;
        }
        
        public Composition(Composition other) {
            this.squaresUsed = other.squaresUsed;
            this.sum = other.sum;
            this.baseCompositions = new ArrayList<>(other.baseCompositions);
        }
        
        public void addSubComposition(Composition composition) {
            this.squaresUsed += composition.squaresUsed;
            this.sum += composition.sum;
            this.baseCompositions.addAll(composition.baseCompositions);
        }
        
        public void removeSubComposition(Composition composition) {
            if (baseCompositions.containsAll(composition.baseCompositions)) {
                this.baseCompositions.removeAll(composition.baseCompositions);
                this.sum -= composition.sum;
                this.squaresUsed -= composition.squaresUsed;
            }
        }
        
        public boolean containsAnyBaseElements(Composition other) {
            for (Composition c : other.baseCompositions) {
                if (baseCompositions.contains(c)) {
                    return true;
                }
            }
            return false;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime
                    * result
                    + ((baseCompositions == null) ? 0 : baseCompositions
                            .hashCode());
            result = prime * result
                    + ((squaresUsed == null) ? 0 : squaresUsed.hashCode());
            result = prime * result + ((sum == null) ? 0 : sum.hashCode());
            return result;
        }

        @Override
        public int compareTo(Composition o) {
            return sum.compareTo(o.sum) == 0 ? -squaresUsed.compareTo(o.squaresUsed) : -sum.compareTo(o.squaresUsed);
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            
            Composition other = (Composition) obj;
            
            if (squaresUsed == null) {
                if (other.squaresUsed != null)
                    return false;
            } else if (!squaresUsed.equals(other.squaresUsed))
                return false;
            
            if (sum == null) {
                if (other.sum != null)
                    return false;
            } else if (!sum.equals(other.sum))
                return false;
            
            if (baseCompositions == null) {
                if (other.baseCompositions != null)
                    return false;
            } else if (!(
                        baseCompositions.containsAll(other.baseCompositions) && 
                        baseCompositions.size() == other.baseCompositions.size()))
                return false;
            
            return true;
        }
    }
    
    public class CompositionMap {
        @Getter private HashMap<Integer, HashMap<Integer, ArrayList<Composition>>> map;
        
        public CompositionMap(int maxSquares) {
            this.map = new HashMap<Integer, HashMap<Integer, ArrayList<Composition>>>();
        }
        
        public Composition getComposition(int sum, int squares) {
            List<Composition> compositions = getCompositionList(sum, squares);
            return compositions == null || compositions.size() == 0 ? null : compositions.get(0);
        }
        
        public List<Composition> getCompositionList(int sum, int squares) {
            return map.get(sum) == null ? null : map.get(sum).get(squares);
        }
        
        public PriorityQueue<Composition> getCompositionsBelow(int sum, int squares) {
            
            PriorityQueue<Composition> result = new PriorityQueue<Composition>();
            
            for (int i=1; i<=sum; i++) {
                for (int j=1; j<=squares; j++) {
                    //  There is no need to put multiple identical (in terms of sum 
                    //  and number of squares) into the return heap.
                    HashMap<Integer, ArrayList<Composition>> compositionMap = map.get(i);
                    if (compositionMap != null) {
                        List<Composition> compositions = compositionMap.get(j);
                        if (compositions != null && compositions.size() > 0) {
                            result.add(compositions.get(0));
                        }
                    }
                }
            }
            
            return result;
        }
        
        public void put(Composition c) {
            if (map.get(c.sum) == null) {
                map.put(c.sum, new HashMap<Integer, ArrayList<Composition>>());
            }
            
            if (map.get(c.sum).get(c.squaresUsed) == null) {
                map.get(c.sum).put(c.squaresUsed, new ArrayList<>());
            }
            
            map.get(c.sum).get(c.squaresUsed).add(c);
        }
    }
    
    Map<Integer, Integer> reverseLookup = new HashMap<>();
    List<Composition> finalCompositions = new ArrayList<>();
    
    PriorityQueue<Composition> baseCompositions = new PriorityQueue<>((c1, c2) -> 
    c1.squaresUsed.compareTo(c2.squaresUsed) == 0 ? 
            -(c1.sum.compareTo(c2.sum)) : -(c1.squaresUsed.compareTo(c2.squaresUsed)));
    
    //  Ordering: first by squares used (largest pieces), second by sum (largest of the largest pieces)
    PriorityQueue<Composition> allCompositions = new PriorityQueue<>((c1, c2) -> 
        c1.squaresUsed.compareTo(c2.squaresUsed) == 0 ? 
                -(c1.sum.compareTo(c2.sum)) : -(c1.squaresUsed.compareTo(c2.squaresUsed)));

    @Test(dataProvider = "dpApproachSets") 
    public void dpApproach(int k, List<Integer> set) {
        
        int n = set.size();
        
        int max = 0;
        int min = 0;
        
        for (int i=n-k; i<=n; i++) {
            max += i * i;
        }
        
        for (int i=1; i<=k; i++) {
            min += i * i;
        }
        
        CompositionMap map = new CompositionMap(k);
        
        //  Seed with the single-square decompositions.
        for (int i=1; i<=n; i++) {
            Composition c = new Composition();
            c.sum = i*i;
            c.squaresUsed = 1;
            c.baseCompositions.add(new Composition(c));
            allCompositions.add(c);
            baseCompositions.add(c);
            reverseLookup.put(i*i, i);
            map.put(c);
        }
        
        System.out.println("Min: " + min + ", Max: " + max);
        
        for (int target=min; target<=max; target++) {
            decompose2(target, k, new Composition(), map);
        }
        
        Collections.sort(finalCompositions, (c1, c2) -> 
        {   Integer sum1 = c1.baseCompositions.stream().mapToInt(c -> c.sum).sum();
            Integer sum2 = c2.baseCompositions.stream().mapToInt(c -> c.sum).sum();
            return sum1.compareTo(sum2);
        });
        
        System.out.println("Results: ");
        for (Composition c : finalCompositions) {
            System.out.println(printCandidate(c));
        }
        System.out.println(finalCompositions.size());
    }
    
    private String printCandidate(Composition c) {
        StringBuilder builder = new StringBuilder();
        builder.append(c.sum + " = ");
        for (Composition s : c.baseCompositions) {
            builder.append(reverseLookup.get(s.sum) + "^2" + " ");
        }
        return builder.toString();
    }
    
    private void decompose2(int sumLeft, int squaresLeft, Composition current, CompositionMap map) {
        
        //  Check for exact match before doing anything complicated.
        Composition exactMatch = map.getComposition(sumLeft, squaresLeft);
        
        if (exactMatch != null) {
            //  Any of the multiple matches is fine.
            current.addSubComposition(exactMatch);
            handleSolution(current, map);
            current.removeSubComposition(exactMatch);
            return;
        }
        
        //  DFS down the largest-pieces-first path.  Composition::compareTo is rigged such that
        //  a PriorityQueue will return Compositions in order first of sum, then squares used.
        //  All will be less than or equal to the remaining sum and squares needed.
        PriorityQueue<Composition> candidates = map.getCompositionsBelow(sumLeft, squaresLeft);
        
        for (Composition candidate : candidates) {
            
            //  The final composition cannot contain any duplicate squares.
            if (current.containsAnyBaseElements(candidate) == false) {
                current.addSubComposition(candidate);
                
                int newSum = sumLeft - candidate.sum;
                int newSquaresLeft = squaresLeft - candidate.squaresUsed;
                
                if (newSum == 0 && newSquaresLeft == 0) {
                    handleSolution(current, map);
                    map.put(new Composition(current));
                } else if (newSum > 0 && newSquaresLeft > 0) {
                    if (baseCompositions.contains(current) == false) {
                        map.put(new Composition(current));
                    }
                    
                    decompose2(newSum, newSquaresLeft, current, map);
                }
                
                current.removeSubComposition(candidate);
            }
        }
    }
    
    private boolean handleSolution(Composition composition, CompositionMap map) {
        if (map.getComposition(composition.sum, composition.squaresUsed) != null) {
            //  The Composition.equalTo does a deep equality check on the base Compositions.
            //  We don't want that for duplicate solution removal.
            finalCompositions.removeIf(c -> c.sum == composition.sum && c.squaresUsed == composition.squaresUsed);
            return false;
        } else {
            finalCompositions.add(new Composition(composition));
            return true;
        }
    }
    
    /*
    
    private void decompose(int target, int maxSquares, Composition composition, PriorityQueue<Composition> remainingCompositions) {
        
        PriorityQueue<Composition> localCompositions = new PriorityQueue<Composition>(
                remainingCompositions.stream()
                    .filter(c -> { return c.sum <= target - composition.sum && c.squaresUsed < maxSquares - composition.squaresUsed; })
                    .collect(Collectors.toList()));
        
        while (localCompositions.size() > 0) {
            
            Composition candidate = localCompositions.remove();
            
            //  Do stuff
            if (composition.containsAnyBaseElements(candidate) == false) {
                
                //  Fail fast if the candidate is too large.
                if (composition.sum + candidate.sum > target || composition.squaresUsed + candidate.squaresUsed > maxSquares) {
                    continue;
                }
                
                //  Special case: if the composition is a base composition, duplicate it.  Otherwise
                //  we will continually clobber the base values.
                if (baseCompositions.contains(candidate)) {
                    candidate = new Composition(candidate);
                }
                
                composition.addSubComposition(candidate);
                
                //  If this is either a new composition, or one of the original base compositions...
                if (allCompositions.contains(composition) == false || baseCompositions.contains(composition)) {
                    
                    //  The composition is completed, add it if new:
                    if (isCompositionComplete(composition, target, maxSquares)) {
                        
                        //  If the composition is already known, remove it from the final set.
                        //  This operation is done less often than the outer .contains(), so leave
                        //  Composition::equalTo alone.
                        List<Composition> matchingCompositions = allCompositions.stream()
                                .filter(c -> { return isCompositionComplete(c, target, maxSquares); })
                                .collect(Collectors.toList());
                                
                        if (matchingCompositions.size() > 0) {
                            System.out.println("Removing duplicate: " + printCandidate(composition));
                            finalCompositions.removeAll(matchingCompositions);
                        } else {
                            System.out.println("Adding new solution: " + printCandidate(composition));
                            allCompositions.add(new Composition(composition));
                            finalCompositions.add(new Composition(composition));
                        }
                    } else {
                        //  Add if new (could be a base composition, so check again here)
                        if (baseCompositions.contains(composition) == false) {
                            allCompositions.add(new Composition(composition));
                        }
                        
                        //  Continue to try and decompose the target if we're still under the
                        //  squares and sum limit:
                        if (composition.squaresUsed < maxSquares && composition.sum < target) {
                            remainingCompositions.remove(candidate);
                            decompose(target, maxSquares, composition, remainingCompositions);
                            remainingCompositions.add(candidate);
                        }
                        
                        //  If we are over the square/sum limit, do nothing.
                    }
                } 

                if (isCompositionComplete(composition, target, maxSquares)) {
                    composition.removeSubComposition(candidate);
                    return;
                }
                
                composition.removeSubComposition(candidate);
            }
        }
    }
    
    */
    
    boolean isCompositionComplete(Composition c, int sum, int squares) {
        return c.sum == sum && c.squaresUsed == squares;
    }
    
    
    @DataProvider
    private Object[][] dpApproachData() {
        return new Object[][] { { 20, 10 } };
    }
    
    @DataProvider
    private Object[][] dpApproachSets() {
        //return new Object[][] { { 3, Arrays.asList(new Integer[] { 1, 3, 6, 8, 10, 11 }) } };
        return new Object[][] { { 5, this.buildConsecutiveIntSet(10) } };
    }
    
    @Test(dataProvider = "sets")
    public void combinatoricApproach(int k, List<Long> set) {
        CombinatoricIterator<Long> iterator = new CombinatoricIterator<>(set, k);
        
        HashMap<Long, List<Long>> sums = new HashMap<>();
        List<Long> sumsHit = new ArrayList<>();
        
        while (iterator.hasNext()) {
            Collection<Long> subset = iterator.next();
            
            long sum = MathHelper.sumLongCollection(subset);
            
            if (sum == 103) {
                System.out.println("103: " + subset);
            }
            
            if (sumsHit.contains(sum) == false) {
                sums.put(sum, new ArrayList<>(subset));
                sumsHit.add(sum);
            } else {
                sums.remove(sum);
            }
        }
        
        List<Long> uniqueSums = new ArrayList<>(sums.keySet());
        Collections.sort(uniqueSums);
        
        List<List<Long>> uniqueSets = new ArrayList<>();
        
        for (Long sum : uniqueSums) {
            uniqueSets.add(sums.get(sum));
        }
        
        Collections.sort(uniqueSets, new ListComparator<Long>());
        
        long sum = MathHelper.sumLongCollection(uniqueSums);
        System.out.println(set.size() + ": " + sum + " - " + uniqueSets.size() + "/" + MathHelper.nChooseK(set.size(), k));
    
        for (Long s : uniqueSums) {
            System.out.println(s + ", hits: " + sums.get(s).size());
        }
    }
    
    @DataProvider
    private Iterator<Object[]> sets() {
        
        List<Object[]> testDataRows = new ArrayList<>();
        
        for (int i = MIN_PROBLEM_SIZE; i <= MIN_PROBLEM_SIZE; i++) {
            Object[] testMethodRow = new Object[] { i, buildConsecutiveLongSet(10) };
            testDataRows.add(testMethodRow);
        }
        
        return testDataRows.iterator();
    }
    
    private List<Long> buildConsecutiveLongSet(int size) {
        
        List<Long> set = new ArrayList<Long>();
        
        for (long i=1; i<=size; i++) {
            set.add(i * i);
        }
        
        return set;
    }

    private List<Integer> buildConsecutiveIntSet(int size) {
        
        List<Integer> set = new ArrayList<Integer>();
        
        for (int i=1; i<=size; i++) {
            set.add(i * i);
        }
        
        return set;
    }
}
