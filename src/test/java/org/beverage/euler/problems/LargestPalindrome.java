package org.beverage.euler.problems;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.beverage.euler.helpers.MathHelper;
import org.beverage.euler.iterators.CombinatoricIterator;
import org.beverage.euler.iterators.PalindromicIterator;
import org.beverage.euler.model.PrimeVector;
import org.testng.annotations.Test;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;

@Test(suiteName = "Euler")
public class LargestPalindrome {

    public void foo() {
        
        PalindromicIterator iter = new PalindromicIterator(998001);
        List<PrimeVector> candidates = new ArrayList<>();
        
        for (long value = iter.next(); iter.hasNext() && value > 100 * 100; value = iter.next()) {
            PrimeVector vector = MathHelper.getPrimeFactors(value);
            candidates.add(vector);
        }
        //  Remove vectors with primes > 999.
        List<PrimeVector> filteredCandidates = 
            candidates.stream()
                .filter(p -> p.getFactors().values().stream().anyMatch(v -> v.getPrime() / 1000 > 0) == false)
                .collect(Collectors.toList());
        
        System.out.println("Total number of candidates: " + filteredCandidates.size());
        PrimeVector left = null;
        PrimeVector right = null;
        
        search:
        for (PrimeVector candidate : filteredCandidates) {
            //  Decompose all caniddates starting with the largest:
            System.out.println("Checking " + candidate);
            Multiset<Long> factors = candidate.getFactorSet();
            
            for (int i=1; i<candidate.getFactorCount(); i++) {
                CombinatoricIterator<Long> iterator = new CombinatoricIterator<Long>(factors, i);
                
                while (iterator.hasNext()) {
                    left = PrimeVector.fromFactors(iterator.next());
                    if (left.getValue() < 100 || left.getValue() > 999) {
                        continue;
                    }
                    
                    right = PrimeVector.fromFactors(difference(factors, left.getFactorSet()));
                    if (right.getValue() > 99 && right.getValue() < 1000) {
                        break search;
                    }
                }
            }
        }
        
        PrimeVector result = new PrimeVector();
        result.addVector(left);
        result.addVector(right);
        
        System.out.println("  Left: " + left);
        System.out.println(" Right: " + right);
        System.out.println("Result: " + result);
    }
    
    public <T> Multiset<T> difference(Multiset<T> left, Multiset<T> right) {
        HashMultiset<T> result = HashMultiset.create(left);
        for (T value : right) {
            result.remove(value);
        }
        return result;
    }
}
