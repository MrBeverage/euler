package org.beverage.euler.problems;

import java.util.ArrayList;
import java.util.List;

import org.beverage.euler.helpers.MathHelper;
import org.beverage.euler.iterators.PrimeIterator2;
import org.testng.annotations.Test;

@Test(suiteName = "Euler")
public class ConsecutivePrimes {

    static final int PROBLEM_SIZE = 1000000;
    
    @Test(enabled = false)
    public void solve() {
        PrimeIterator2 iterator = new PrimeIterator2(1000000);
        
        long maxPrime = 1l;
        long primeCount = 0l;
        
        while (iterator.hasNext()) {
            maxPrime = iterator.next();
            primeCount++;
            System.out.println(maxPrime);
        }
        
        System.out.println("Max: " + maxPrime);
        System.out.println("Count: " + primeCount);
    }
    
    public void first21() {
        PrimeIterator2 iterator = new PrimeIterator2(PROBLEM_SIZE);
        List<Long> primes = new ArrayList<>();
        while (iterator.hasNext()) {
            primes.add(iterator.next());
        }
        
        int maxSize = 0;
        int maxSum  = 0;
        int sum = 0;
        int size = 0;
        
        searchLoop:
        for (int i=0; i<primes.size(); i++) {
            sum = 0;
            size = 0;
            
            for (int j=i; j<primes.size() && sum < PROBLEM_SIZE; j++) {
                if (primes.size() - i - j < maxSize) {
                    //  At this point, whatever is the current maximum is the winner.
                    break searchLoop;
                }
                
                if (sum + primes.get(j) > PROBLEM_SIZE) {
                    break;
                }
                
                sum += primes.get(j);
                size++;
                
                if (size > maxSize && MathHelper.isPrime((long)sum)) {
                    maxSum = sum;
                    maxSize = j - i;
                }
            }
        }
        
        System.out.println("       Final sum: " + maxSum);
        System.out.println("Number of primes: " + maxSize + 1);
        System.out.println(primes.size());
    }
}
