package org.beverage.euler.problems;

import org.beverage.euler.helpers.MathHelper;
import org.beverage.euler.model.PrimeVector;
import org.testng.annotations.Test;

@Test(suiteName = "Euler")
public class MusicGroups {

    public void foo() {
        long mod = 1000000007l;
        
        for (int i=0; i<4; i++) {
            PrimeVector vector = MathHelper.getPrimeFactors(509089824l + i * mod);
            System.out.println(vector);
        }
    }
}
