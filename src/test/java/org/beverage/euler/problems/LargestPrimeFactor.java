package org.beverage.euler.problems;

import java.util.ArrayList;
import java.util.List;

import org.beverage.euler.iterators.PrimeIterator;
import org.testng.annotations.Test;

@Test(suiteName = "Euler")
public class LargestPrimeFactor {

    public static long TEST_VALUE = 600851475143l;

    public void getLargestPrimeFactor() {
        PrimeIterator iterator = new PrimeIterator();

        List<Long> factors = new ArrayList<>();
        long test = TEST_VALUE;

        while (iterator.hasNext() && test > 1) {
            long prime = iterator.next();

            while (test % prime == 0) {
                if (factors.contains(prime) == false) {
                    factors.add(prime);
                }
                test /= prime;
            }
        }

        System.out.println(factors.get(factors.size() - 1));
    }
}
