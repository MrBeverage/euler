package org.beverage.euler.problems;

import org.beverage.euler.iterators.PrimeIterator2;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

@Test(suiteName = "Euler")
public class MiscPrimeProblems {

    @Test(description = "Problem 7", dataProvider = "problemSizes")
    public void nthPrime(int problemSize) {
        PrimeIterator2 iter = new PrimeIterator2(problemSize * problemSize + 1);
        long prime = 0l;
        
        for (int i=0; i<problemSize; i++) {
            prime = iter.next();
        }
        
        System.out.println(problemSize + ": " + prime);
    }
    
    @Test(description = "Problem 9", dataProvider = "sums")
    public void sumPrimes(int numPrimes) {
        PrimeIterator2 iter = new PrimeIterator2(numPrimes);
        
        long sum = 0l;
        
        for (int i=0; i<numPrimes && iter.hasNext(); i++) {
            Long nextPrime = iter.next();
            if (nextPrime != null) {
                sum += nextPrime;
            }
        }
        
        System.out.println(sum);
    }
    
    @DataProvider
    private Object[][] problemSizes() {
        return new Object[][] { { 2 }, { 4 }, { 10 }, { 4 }, { 10001 } }; 
    }
    
    @DataProvider
    private Object[][] sums() {
        return new Object[][] { { 10 }, { 2000000 } };
    }
}
