package org.beverage.euler.problems;

import org.beverage.euler.helpers.MathHelper;
import org.beverage.euler.model.PrimeVector;
import org.beverage.euler.model.PrimeVector.PrimeFactor;
import org.testng.annotations.Test;

@Test(suiteName = "Euler")
public class SmallestMultiple {

    int number = 20;
    
    public void getSmallestMultiple() {
        PrimeVector smallestMultiple = new PrimeVector();
        
        //  The smallest multiple divisible by 1..N is all prime factors below N times
        //  the maximum number of times they are used by any particular number below N.
        //  For example, 1..20 is 2^4 * 3^2 * 5 * 7 * 11 * 13 * 17 * 19.  2^4 == 16 and 
        //  3 ^ 2 == 9 - all other prime factors below 20 are used at most once.
        //
        //  We can use the existing PrimeVector's setFactor to build a minimum value that
        //  satisfies this constraint.
        for (int i=2; i<=number; i++) {
            System.out.println("Checking " + i);
            PrimeVector iVector = MathHelper.getPrimeFactors(i);
            
            for (PrimeFactor factor : iVector.getFactors().values()) {
                PrimeFactor smallestMultipleFactor = smallestMultiple.getFactor(factor.getPrime());
                
                if (smallestMultipleFactor == null || smallestMultipleFactor.getCount() < factor.getCount()) {
                    smallestMultiple.setFactor(factor);
                }
            }
        }
        
        System.out.println(smallestMultiple);
    }
}
