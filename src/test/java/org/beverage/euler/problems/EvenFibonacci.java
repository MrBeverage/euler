package org.beverage.euler.problems;

import org.beverage.euler.iterators.FibonacciIterator;
import org.testng.annotations.Test;

@Test(suiteName = "Euler")
public class EvenFibonacci {

    private static final long MAX_VALUE = 4000000l;

    public void sum() {
        FibonacciIterator iterator = new FibonacciIterator();
        long result = 0l;

        while (iterator.hasNext()) {
            long value = iterator.next();

            if (value > MAX_VALUE) {
                break;
            }

            System.out.println(value);

            if (value % 2 == 0) {
                result += value;
            }
        }

        System.out.println("Result: " + result);
    }
}
