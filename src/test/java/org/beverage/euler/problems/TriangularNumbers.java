package org.beverage.euler.problems;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.IteratorUtils;
import org.beverage.euler.helpers.MathHelper;
import org.beverage.euler.iterators.CombinatoricIterator;
import org.beverage.euler.model.PrimeVector;
import org.testng.annotations.Test;

@Test(suiteName = "Euler")
public class TriangularNumbers {

    @SuppressWarnings("unchecked")
    @Test(description = "Problem 12")
    public void findDivisors() {
        
        int triangularNumber = 0;
        Set<Long> divisors = new HashSet<>();
        
        for (int i=1; divisors.size() < 500; i++) {
            triangularNumber += i;
            PrimeVector vector = MathHelper.getPrimeFactors(triangularNumber);
            
            List<Long> primeFactors = IteratorUtils.toList(vector.iterator());
            divisors = new HashSet<>();
            
            for (int j=1; j<=primeFactors.size(); j++) {
                CombinatoricIterator<Long> combinations = new CombinatoricIterator<>(primeFactors, j);
                while (combinations.hasNext()) {
                    divisors.add(MathHelper.collectionProduct(combinations.next()));
                }
            }
        }
        
        System.out.println(triangularNumber + ": " + divisors);
    }
}
