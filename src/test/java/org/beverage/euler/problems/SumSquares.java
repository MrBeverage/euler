package org.beverage.euler.problems;

import org.beverage.euler.helpers.MathHelper;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

@Test(suiteName = "Euler")
public class SumSquares {

    @Test(dataProvider = "sums")
    public void sumSquares(long max) {
        
        long sumSquares = 0l;
        long squareSums = MathHelper.simpleSummation(max) * MathHelper.simpleSummation(max);
        
        for (long i=1; i<=max; i++) {
            sumSquares += i * i;
        }
        
        System.out.println(squareSums + " - " + sumSquares + " = " + (squareSums - sumSquares));
    }
    
    @DataProvider
    public Object[][] sums() {
        return new Object[][] { { 10 }, { 100 } };
    }
}
