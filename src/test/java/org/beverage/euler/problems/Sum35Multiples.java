package org.beverage.euler.problems;

import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@Test(suiteName = "Euler", testName = "Sum of multiples of 3 and 5")
public class Sum35Multiples {

    private static final long MAX = 999;
    private long bruteForceResult = 0l;
    private long summationResult = 0l;

    public void bruteForce() {
        for (int i = 0; i <= MAX; i++) {
            if (i % 3 == 0 || i % 5 == 0) {
                bruteForceResult += i;
            }
        }

        System.out.println("Result: " + bruteForceResult);
    }

    public void summation() {
        summationResult = sum(3, MAX) + sum(5, MAX) - sum(15, MAX);

        System.out.println("Result: " + summationResult);
    }

    @Test(dependsOnMethods = { "bruteForce", "summation" })
    public void check() {
        assertThat(summationResult, is(bruteForceResult));
    }

    private long sum(long step, long max) {
        long n = Math.floorDiv(max, step);
        return (step * n * (n + 1)) / 2;
    }
}
