package org.beverage.euler.tests;

import org.beverage.euler.iterators.PalindromicIterator;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@Test(suiteName = "Euler")
public class Palindromes {

    public static long TEST_VALUE = 600851475143l;

    @Test(dataProvider = "palindromes")
    public void nextPalindrome(long palindrome, long expectedResult) {
        PalindromicIterator dec = new PalindromicIterator(palindrome);
        long result = dec.next();
        assertThat(result, is(expectedResult));
    }

    @Test(dataProvider = "maxPalindromes")
    public void maxPalindrome(long order, long expectedResult) {
        PalindromicIterator dec = new PalindromicIterator(0);
        long result = dec.getMaxPalindromeForOrder(order);
        assertThat(result, is(expectedResult));
    }

    @Test(dataProvider = "minPalindromes")
    public void minPalindrome(long order, long expectedResult) {
        PalindromicIterator dec = new PalindromicIterator(0);
        long result = dec.getMinPalindromeForOrder(order);
        assertThat(result, is(expectedResult));
    }
    
    @Test(dataProvider = "merge") 
    public void merge(long left, long right, long order, long expectedResult) {
        PalindromicIterator pi = new PalindromicIterator(0);
        long result = pi.mergeLeftRight(left, right, order);
        assertThat(result, is(expectedResult));
    }

    @DataProvider(name = "palindromes")
    private Object[][] palindromes() {
        return new Object[][] { 
                { 1000000, 999999 },
                { -999999, -1000001 },
                { 100000, 99999 }, 
                { -99999, -100001 },
                { 99999, 99899 },
                { 98700, 98689 },
                { -9987, -9999 },
                { 21, 11 },
                { 12, 11 },
                { 11, 9 },
                { 10, 9 },
                { 2, 1 },
                { 1, 0 },
                { 0, -1 },
                { -12, -22 },
                { -11, -22 },
                { -10, -11 },
                { -9, -10 },
                { -2, -3 },
                { -1, -2 },
                { 999998, 998899 },
                { 888889, 888888 },
                { 99998, 99899 },
                { 88889, 88888 },
                { -99998, -99999 },
                { -88889, -88988 },
        };
    }

    @DataProvider(name = "maxPalindromes")
    private Object[][] maxPalindromes() {
        return new Object[][] { { 4, 99999 }, { 0, 9 } };
    }

    @DataProvider(name = "minPalindromes")
    private Object[][] minPalindromes() {
        return new Object[][] { { 4, 10001 }, { 0, 1 }, { 1, 11 } };
    }
    
    @DataProvider(name = "merge")
    private Object[][] merge() {
        return new Object[][] { 
                { 998, 899, 5, 998899 }, 
                { 998, 899, 4, 99899 },
                { 1, 1, 1, 11 },
                // { 0, 9, 0, 9 }
        };
    }
}
