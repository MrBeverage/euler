package org.beverage.euler.tests;

import org.beverage.euler.helpers.MathHelper;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@Test(suiteName = "Unit Tests")
public class ReverseNumber {

    @Test(dataProvider = "numbers")
    public void reverseNumbers(long number, long expectedResult) {
        long result = MathHelper.reverseBase10Number(number);

        assertThat(result, is(expectedResult));
    }

    @DataProvider(name = "numbers")
    public Object[][] numbers() {
        return new Object[][] { { 12345l, 54321l }, { 1l, 1l }, { 0l, 0l },
                { -1l, -1l }, { -12345l, -54321l }, };
    }
}
