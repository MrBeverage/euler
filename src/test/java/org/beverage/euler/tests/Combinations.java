package org.beverage.euler.tests;

import org.beverage.euler.helpers.MathHelper;
import org.beverage.euler.iterators.CombinatoricIterator;
import org.beverage.euler.model.PrimeVector;
import org.testng.annotations.Test;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;

@Test(suiteName = "Unit Tests")
public class Combinations {
    
    public void multisetCombinations() {
        Multiset<Long> primes = HashMultiset.create(MathHelper.getPrimeFactors(9009));
        System.out.println(primes);
        
        CombinatoricIterator<Long> iter = new CombinatoricIterator<>(primes, 3);
        while (iter.hasNext()) {
            Multiset<Long> setA = HashMultiset.create(iter.next());
            Multiset<Long> setB = HashMultiset.create(primes);
            for (Long value : setA) {
                setB.remove(value);
            }
            
            PrimeVector aV = PrimeVector.fromFactors(setA.toArray(new Long[] { }));
            PrimeVector bV = PrimeVector.fromFactors(setB.toArray(new Long[] { }));
            
            System.out.println(aV);
            System.out.println(bV);
        }
        
    }
}
