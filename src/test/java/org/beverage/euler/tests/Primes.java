package org.beverage.euler.tests;

import java.util.ArrayList;
import java.util.List;

import org.beverage.euler.helpers.MathHelper;
import org.beverage.euler.iterators.PrimeIterator2;
import org.beverage.euler.model.PrimeVector;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@Test(suiteName = "Unit Tests")
public class Primes {

    public void iterator() {
        PrimeIterator2 iterator = new PrimeIterator2(100);
        
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        
        PrimeIterator2 iterator2 = new PrimeIterator2(200);
        
        while (iterator2.hasNext()) {
            System.out.println(iterator2.next());
        }
    }
    
    public void decompose() {
        
        PrimeVector vector = MathHelper.getPrimeFactors(9009);
        
        System.out.println(vector.toString());
    }
    
    public void addition() {
        @SuppressWarnings("serial")
        List<PrimeVector> vectors = new ArrayList<PrimeVector>() {{
            add(MathHelper.getPrimeFactors(99));
            add(MathHelper.getPrimeFactors(91));
        }};
        
        PrimeVector vector = new PrimeVector(vectors);
        
        System.out.println(vector.toString());
        System.out.println(vector.getValue());
        
        assertThat(vector.getValue(), is(9009l));
    }
    
    @Test(dataProvider = "compositePrimes")
    public void composite(Long[] powers, long expectedResult) {
        PrimeVector vector = PrimeVector.fromPowerVector(powers);
        System.out.println(vector);
        assertThat(vector.getValue(), is(expectedResult));
    }
    
    @DataProvider
    private Object[][] compositePrimes() {
        return new Object[][] { 
                { new long[] { 1, 0, 2, 0, 1 }, 550l },
                { new long[] { 1 }, 2l },
                { new long[] { }, 1l },
                { new long[] { 0, 1 }, 3l }
        };
    }
}
